class A:
    def __init__(self):
        self._callback = None

    def onEvent(self, callback):
        self._callback = callback
        return callback


a = A()


@a.onEvent
def Callback():
    print('Callback()')


