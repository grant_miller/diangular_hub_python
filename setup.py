from setuptools import setup
import subprocess

# this grabs the requirements from requirements.txt
REQUIREMENTS = []
for line in open("requirements.txt").readlines():
    line = line.strip()
    if not line.startswith('#'):
        REQUIREMENTS.append(line)
print('REQUIREMENTS=', REQUIREMENTS)

output = subprocess.check_output('pip3 install git+https://github.com/GrantGMiller/simple_tcp.git@master#egg=simple_tcp'.split())
print('subprocess output=', output)

setup(
    name='dng',
    version='0.0.0',
    packages=['dng'],
    url='https://bitbucket.org/grant_miller/diangular_hub_python/src',
    license='The MIT License',
    author='Grant Miller',
    author_email='grant@grant-miller.com',
    description='Hub for Diangular',
    install_requires=REQUIREMENTS
)
