'''
Install this with:
pip3 install update https://bitbucket.org/grant_miller/diangular_hub_python/get/master.tar.gz

Uninstall, then reinstall
pip3 uninstall dng -y && pip3 install update https://bitbucket.org/grant_miller/diangular_hub_python/get/master.tar.gz

to uninstall:
pip3 uninstall dng -y

How to auto-start a python script when the Raspi boots:
https://www.wikihow.com/Execute-a-Script-at-Startup-on-the-Raspberry-Pi
'''
from dng.hub import Hub

hub = Hub()
print('hub.Devices=', hub.Devices)

DEBUG = True

DELETE_DB_ON_STARTUP = False
if DEBUG:
    if DELETE_DB_ON_STARTUP:
        import os

        try:
            os.remove('Mydatabase.db')
        except Exception as e:
            print('9', e)

    API_URL = TEST_URL = "wss://echo.websocket.org"
else:
    API_URL = 'https://diangular.com'

if DELETE_DB_ON_STARTUP:
    hub.AddDevice(
        name='IPI204',
        hostname='10.8.27.171',
        ipport=23
    )
print('end main.py')

