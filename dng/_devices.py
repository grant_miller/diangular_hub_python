from dictabase import (
    BaseDictabaseTable,
    FindOne,
    Delete
)

from simple_tcp.tcp_client import SimpleTCPClient


class _Device(BaseDictabaseTable):
    pass


class DeviceTCP:
    def __init__(self, *args, **kwargs):
        print('DeviceTCP(args=', args, ', kwargs=', kwargs)
        self._dev = FindOne(_Device, **kwargs)
        if self._dev is None:
            print('creating new _Device')
            kwargs.pop('id', None)
            newDev = _Device(**kwargs)
            print('newDev=', newDev)
            self._dev = newDev
        else:
            print('restoring _Device self._dev=', self._dev)
        self._dev['protocol'] = 'TCP'

        self._interface = SimpleTCPClient(
            hostname=self._dev['hostname'],
            ipport=self._dev['ipport']
        )
        self._interface.onConnected = self.onConnected
        self._interface.onDisconnected = self.onDisconnected

    def Send(self, data):
        self._interface.Send(data)

    @property
    def onReceive(self):
        return self._interface.onReceive

    @onReceive.setter
    def onReceive(self, callback):
        def NewCallback(_, data):
            callback(self, data)

        self._interface.onReceive = NewCallback
        return callback

    @property
    def onConnected(self):
        return self._interface.onConnected

    @onConnected.setter
    def onConnected(self, callback):
        self._interface.onConnected = callback
        self._dev['connectionStatus'] = self._interface.ConnectionStatus

    @property
    def onDisconnected(self):
        return self._interface.onDisconnected

    @onDisconnected.setter
    def onDisconnected(self, callback):
        self._interface.onDisconnected = callback
        self._dev['connectionStatus'] = self._interface.ConnectionStatus

    def Delete(self):
        # remove from database
        Delete(self._dev)

    def __iter__(self):
        for k, v in self._dev.items():
            if not k.startswith('_'):
                yield k, v
