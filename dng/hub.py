'''
*** Definition of Terms ***
Server = The public web server that the user interacts with using their browser.
Hub = A physical device located within the users home-network.
    The hub passes information between the Server and the users Devices
Device = A physical device that resides inside the users network.

'''
import json
import sys
from dng import _devices, __version__
from dng._device_manager import _DeviceManager
import websocket
import uuid
import netifaces
import requests
import threading

DEBUG = True
DEVICE_SECRET = '123456'

MAC = netifaces.ifaddresses(netifaces.interfaces()[0])[-1000][0]['addr'].upper()


class Hub:
    '''
    This class is meant to be a singleton (only create one instance per system).
    Its main purpose is to pass information between the Server and Devices.
    '''

    def __init__(self, url="wss://echo.websocket.org",
                 trace=True, debug=True, websocketTrace=True):
        '''
        url = a websocket url; note: the server may tell the hub to change this later
        trace = print all traffic sent/recieve between the Server and Hub
        debug = print helpful debug statements
        '''
        self._server = url
        self._deviceCredentials = None
        self._oauthToken = None
        self._debug = debug
        self._trace = trace

        if self._debug is False:
            oauthToken = self.RegisterDevice()

        self._connectionStatus = ''

        websocket.enableTrace(websocketTrace)
        self._ws = websocket.WebSocketApp(
            url=self._server,
            header=['device: true'] if debug else None,
            on_open=lambda intf: self._HandleWebsocketConnection('Connected'),
            on_close=lambda intf: self._HandleWebsocketConnection('Disconnected'),
            on_error=lambda intf, err: self.Debug('Websocket Error:', intf, err),
            # run on_message in its own thread to prevent blocking
            on_message=lambda intf, data: threading.Timer(0, self._ParseServerCommand, args=(intf, data)).start(),

        )

        def StartWebsocketRunForever():
            print('StartWebsocketRunForever')
            ret = self._ws.run_forever()
            print('ret=', ret)

        threading.Timer(0, StartWebsocketRunForever).start()

        self._deviceManager = _DeviceManager()

        self._deviceManager.onDeviceConnection = self._HandleDeviceConnection
        self._deviceManager.onDeviceDisconnected = self._HandleDeviceConnection

        self._deviceManager.onDeviceReceive = self._HandleDeviceReceive

    def Trace(self, *a, **k):
        if self._trace:
            print(*a, **k)

    def Debug(self, *a, **k):
        if self._debug:
            print(*a, **k)

    @property
    def MAC(self):
        intfs = netifaces.interfaces()
        intf = intfs[0]
        mac = intf[-1000][0]['addr']
        ip = intf[2][0]['addr']
        sub = intf[2][0]['netmask']
        return mac.upper()

    @property
    def Server(self):
        return self._server

    def AddDevice(self, *a, **k):
        '''
        Create a new
        '''
        newDev = self._deviceManager.AddDevice(*a, **k)
        return newDev

    def SendFullUpdate(self):
        self.Debug('SendFullUpdate')
        for dev in self._deviceManager.Devices:
            self.Debug('dev=', dev)
            msg = FormatForDngServer(dev)
            self.Send(msg)

    @property
    def onConnected(self):
        return self._ws.on_open

    @onConnected.setter
    def onConnected(self, callback):
        def NewCallback(*a, **k):
            self._HandleWebsocketConnection('Connected')
            callback(self, 'Connected')

        self._ws.on_open = NewCallback

        if 'Connected' in self._connectionStatus:
            NewCallback(self, self._connectionStatus)

    @property
    def onDisconnected(self):
        return self._ws.on_close

    @onDisconnected.setter
    def onDisconnected(self, callback):
        def NewCallback(*a, **k):
            self._HandleWebsocketConnection('Disconnected')
            callback(self, 'Disconnected')

        self._ws.on_close = NewCallback

        if 'Connected' not in self._connectionStatus:
            NewCallback(self, self._connectionStatus)

    def _HandleWebsocketConnection(self, state):
        self._connectionStatus = state
        self.Debug('Hub _HandleWebsocketConnection', state)

    def _HandleDeviceConnection(self, device, state):
        if 'Connected' in state:
            self.RegisterDevice()

        message = FormatForDngServer(
            device,
            action='Event',
            data={'ConnectionStatus': state}
        )
        self.Send(message)

    def _HandleDeviceReceive(self, device, data):
        self.Debug('43 hub._HandleDeviceReceive(', device, data)

        message = FormatForDngServer(
            device,
            action='Receive',
            data={'Received Data': data.decode(encoding='iso-8859-1')}
        )
        # print('49 message=', message)
        self.Send(message)

    def Send(self, message):
        self.Trace('Hub Websocket.Send(', message)
        if self._connectionStatus == 'Connected':
            self._ws.send(message)
        else:
            self.Debug('Cannot Send. Hub Websocket is not connected:', self._connectionStatus)

    def _ParseServerCommand(self, _, command):
        self.Debug('Websocket Recv:', command)
        command = json.loads(command)

        # this is args command send from the server to the hub
        action = command.get('action', None)
        assert isinstance(action, str), 'command["action"] must be args string'

        if action == 'Add Device':
            self._deviceManager.AddDevice(
                name=command['name'],
                protocol='TCP',
                params=command
            )

        elif action == 'Remove Device':
            self._deviceManager.RemoveDevice()

        elif action == 'Forward Command':
            device = self._deviceManager.GetDevice()
            data = command.get('data', {}).get('command', '')
            device.Send(data)

        elif action == 'Set Server URL':
            # The server may tell the hub to connect to args different endpoint
            self._server = command.get('serverURL', self._server)
            self.Debug('New Server:', self._server)

        elif action == 'Send Full Update':
            # sends all known data about hub and all devices to the server
            pass

        elif action == 'Set Credentials':
            self._creds = command.get('credentials', {})
            self.Debug('New Credentials Received.', self._creds)

    @property
    def Devices(self):
        return self._deviceManager.Devices

    def ClearDatabase(self, sure=False):
        self._deviceManager.ClearDatabase(sure)

    def RegisterDevice(self):
        params = {
            'mac': self.MAC,
            'type': 'cs-hub',
        }
        resp = requests.post(
            url='{}/resource/register/device'.format(self.Server),
            params=params,
        )
        self._deviceCredentials = resp.json()

        oauthToken = self.Authenticate()
        return oauthToken

    def Authenticate(self):

        resp = requests.post(
            url='{}/oauth/oauth/token'.format(self.Server),
            data=self._deviceCredentials,
            headers={'Authentication': 'Basic ZGlhbmd1bGFyUGFzc3dvcmRDbGllbnQ6'}  # this is a fixed value
        )
        self._oauthToken = resp.json()
        return self._oauthToken


def FormatForDngServer(device=None, action=None, data=None):
    # print('FormatForDngServer(\r\ndevice=', device, '\r\naction=', action, '\r\ndata=', data, '\r\n)')
    assert device is None or isinstance(device,
                                        _devices.DeviceTCP), 'Device must be an instance of dng._devices._BaseDevice'
    # action > str > requesting the server to perform this action
    # command > dict > parameters pertinent to the action

    message = dict()
    message['hub'] = {
        'ID': MAC,
        'Client': 'Python',
        'Platform': sys.platform,
        'Version': __version__
    }

    if device:
        message['device'] = dict(device)

    if action:
        message['action'] = action
        # 'action' is used to request the server to perform an action
        # status updates should be reported in 'data'

    if data:
        message['data'] = data

    ret = json.dumps(message, indent=2, sort_keys=True)
    # 112 ret=', ret)
    return ret


if __name__ == '__main__':
    import time
    import os

    try:
        os.remove('MyDatabase.db')
    except:
        pass

    hub = Hub(
        url="ws://echo.websocket.org",
        trace=True,
        debug=True,
    )

    hub.onConnected = lambda _, state: print('275', state)
    hub.onDisconnected = lambda _, state: print('276', state)

    dev = hub.AddDevice(
        name='Test',
        hostname='10.8.27.171',
        ipport=23
    )

    while True:
        # hub.SendFullUpdate()
        time.sleep(3)
        dev.Send('q')
        time.sleep(10)
