from dng._devices import _Device, DeviceTCP
from dictabase import FindOne, FindAll, Delete, Drop


class _DeviceManager:
    def __init__(self):
        self._devices = []

        self._onDeviceReceiveCallback = None
        self._onDeviceConnectedCallback = None
        self._onDeviceDisconnectedCallback = None

        self._LoadDeviceFromDB()

    def _LoadDeviceFromDB(self):
        print('DeviceManager._LoadDeviceFromDB')
        for device in FindAll(_Device):
            print('20 device', device)
            if device.get('protocol', None) == 'TCP':
                device = DeviceTCP(**device)
                self._RegisterDeviceEvents(device)
                self._devices.append(device)
                print('Loaded Device:', device)

    @staticmethod
    def ClearDatabase(sure=False):
        if sure:
            Drop(_Device)

    def AddDevice(self, name, **kwargs):
        newDevice = DeviceTCP(
            name=name,
            hostname=kwargs.get('hostname'),
            ipport=kwargs.get('ipport', 23)
        )
        self._devices.append(newDevice)
        self._RegisterDeviceEvents(newDevice)
        return newDevice

    @property
    def onDeviceConnected(self):
        return self._onDeviceConnectedCallback

    @onDeviceConnected.setter
    def onDeviceConnected(self, callback):
        self._onDeviceConnectedCallback = callback

    @property
    def onDeviceDisconnected(self):
        return self._onDeviceDisconnectedCallback

    @onDeviceDisconnected.setter
    def onDeviceDisconnected(self, callback):
        self._onDeviceDisconnectedCallback = callback

    @property
    def onDeviceReceive(self):
        return self._onDeviceReceiveCallback

    @onDeviceReceive.setter
    def onDeviceReceive(self, callback):
        self._onDeviceReceiveCallback = callback

    def HandleConnectionEvent(self, device, state):
        if self._onDeviceConnectedCallback:
            self._onDeviceDisconnectedCallback(device, state)

    def HandleReceive(self, device, data):
        print('DeviceManager.HandleReceive(', device, data)
        print('self._onDeviceReceiveCallback=', self._onDeviceReceiveCallback)
        if self._onDeviceReceiveCallback:
            print('62 self._onDeviceReceiveCallback=', self._onDeviceReceiveCallback)
            print('63 calling _onDeviceReceiveCallback(', device, data)
            self._onDeviceReceiveCallback(device, data)

    def _RegisterDeviceEvents(self, device):
        device.onReceive = self.HandleReceive
        device.onConnected = self.HandleConnectionEvent
        device.onDisconnected = self.HandleConnectionEvent

    def RemoveDevice(self, device):
        self._devices.remove(device)
        device.Delete()

    @property
    def Devices(self, ):
        return self._devices.copy()

    def GetDevice(self, **kwargs):
        for device in self._devices:
            device = dict(device)
            if kwargs.items() <= device.items():
                return device
